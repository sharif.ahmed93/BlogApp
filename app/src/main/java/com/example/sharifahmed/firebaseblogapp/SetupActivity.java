package com.example.sharifahmed.firebaseblogapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class SetupActivity extends AppCompatActivity {


    ImageButton displayImageBTN;
    EditText displayNameET;
    Button submitBTN;

    Uri imageUri = null;

    private static final int GALLARY_REQUEST = 3;

    FirebaseAuth firebaseAuthSetup;
    DatabaseReference databaseReferenceUsers;
    StorageReference storageReferenceUserImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        displayImageBTN = (ImageButton) findViewById(R.id.displayImageButtonId);
        displayNameET = (EditText) findViewById(R.id.displayNameEditTextId);
        submitBTN = (Button) findViewById(R.id.displaySubmitButtonId);


        firebaseAuthSetup = FirebaseAuth.getInstance();
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");
        storageReferenceUserImage = FirebaseStorage.getInstance().getReference("user_image");


        displayImageBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent gallaryIntent = new Intent();
                gallaryIntent.setAction(Intent.ACTION_GET_CONTENT);
                gallaryIntent.setType("image/*");
                startActivityForResult(gallaryIntent,GALLARY_REQUEST);
            }
        });

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startSetupAccount();
            }
        });

    }

    private void startSetupAccount() {

        final String displayName = displayNameET.getText().toString().trim();
        final String user_id = firebaseAuthSetup.getCurrentUser().getUid();

        if(!TextUtils.isEmpty(displayName) && imageUri !=null){
            StorageReference fileUser = storageReferenceUserImage.child(imageUri.getLastPathSegment());
            fileUser.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    String photoUrlDownload = taskSnapshot.getDownloadUrl().toString();
                    databaseReferenceUsers.child(user_id).child("userName").setValue(displayName);
                    databaseReferenceUsers.child(user_id).child("userImage").setValue(photoUrlDownload);


                    Intent intent = new Intent(SetupActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLARY_REQUEST && resultCode == RESULT_OK){

            imageUri = data.getData();
            displayImageBTN.setImageURI(imageUri);

/*
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
*/
        }
/*
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
*/
    }
}
