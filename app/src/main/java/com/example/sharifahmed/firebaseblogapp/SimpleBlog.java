package com.example.sharifahmed.firebaseblogapp;

import android.app.Application;
import android.content.res.Configuration;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by SHARIF AHMED on 05-Jun-17.
 */
public class SimpleBlog extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
