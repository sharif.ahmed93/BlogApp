package com.example.sharifahmed.firebaseblogapp;

/**
 * Created by SHARIF AHMED on 03-Jun-17.
 */
public class Post {

    private String postId;
    private String postTitle;
    private String postDescription;
    private String postImage;
    private String userId;
    private String postAdminName;

    public Post() {

    }

    public Post(String postId, String postTitle, String postDescription, String postImage, String userId, String postAdminName) {
        this.postId = postId;
        this.postTitle = postTitle;
        this.postDescription = postDescription;
        this.postImage = postImage;
        this.userId = userId;
        this.postAdminName = postAdminName;
    }

    public String getPostId() {
        return postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostAdminName() {
        return postAdminName;
    }

    public void setPostAdminName(String postAdminName) {
        this.postAdminName = postAdminName;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostDescription() {
        return postDescription;
    }

    public void setPostDescription(String postDescription) {
        this.postDescription = postDescription;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }
}
