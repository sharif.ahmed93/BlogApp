package com.example.sharifahmed.firebaseblogapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class PostActivity extends AppCompatActivity {

    ImageButton postImageIB;
    EditText postTitleET;
    EditText postDescriptioneET;
    Button submitPostBTN;

    private static final int GALLARY_REQUEST = 1;

    Uri imageUri = null;

    StorageReference mStorageRootRef;
    DatabaseReference databaseReferenceBlog;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseCurrentUser;
    DatabaseReference databaseReferenceUsers;

    ProgressDialog progressDialog;

    Uri downloadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        postImageIB = (ImageButton) findViewById(R.id.addPostImageButtonId);
        postTitleET = (EditText) findViewById(R.id.addTitleEditTextId);
        postDescriptioneET = (EditText) findViewById(R.id.addDescriptionEditTextId);
        submitPostBTN = (Button) findViewById(R.id.submitPostButtonId);

        progressDialog = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseCurrentUser = firebaseAuth.getCurrentUser();

        mStorageRootRef = FirebaseStorage.getInstance().getReference();//firebase storage link ref not child ref
        //databaseReference = FirebaseDatabase.getInstance().getReference().child("blog");
        databaseReferenceBlog = FirebaseDatabase.getInstance().getReference("blog");
        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users").child(firebaseCurrentUser.getUid());

        postImageIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gallaryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                gallaryIntent.setType("image/*");
                startActivityForResult(gallaryIntent,GALLARY_REQUEST);
            }
        });

        submitPostBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postDataToSerever();
            }
        });

    }

    private void postDataToSerever() {

        progressDialog.setMessage("Posting to blog......");
        progressDialog.show();

        final String postTitle = postTitleET.getText().toString().trim();
        final String postDescription = postDescriptioneET.getText().toString().trim();

        if(!TextUtils.isEmpty(postTitle) && !TextUtils.isEmpty(postDescription) && imageUri !=null){

            StorageReference filePath = mStorageRootRef.child("blog_images").child(imageUri.getLastPathSegment());

            filePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                   downloadUrl = taskSnapshot.getDownloadUrl();

                    //DatabaseReference newPost = databaseReference.push();

/*
                    newPost.child("post_title").setValue(postTitle);
                    newPost.child("post_description").setValue(postDescription);
                    newPost.child("post_image").setValue(downloadUrl.toString());
*/

                    databaseReferenceUsers.addValueEventListener(new ValueEventListener() {


                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            String postId =  databaseReferenceBlog.push().getKey();

                            String userId = firebaseCurrentUser.getUid();

                            String postAdminName = dataSnapshot.child("userName").getValue().toString();

                            Post singlePost = new Post(postId,postTitle,postDescription,downloadUrl.toString(),userId,postAdminName);
                            databaseReferenceBlog.child(postId).setValue(singlePost).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){

                                        Intent intent = new Intent(PostActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


                    progressDialog.dismiss();

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLARY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            postImageIB.setImageURI(imageUri);
        }
    }
}
