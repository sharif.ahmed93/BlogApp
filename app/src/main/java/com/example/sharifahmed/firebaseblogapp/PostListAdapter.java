package com.example.sharifahmed.firebaseblogapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 5/27/2017.
 */
public class PostListAdapter extends ArrayAdapter<Post> {

    private Context context;
    ArrayList<Post> postLists;

    public PostListAdapter(Context context, ArrayList<Post> postLists) {
        super(context, R.layout.custom_list_layout,postLists);
        this.context = context;
        this.postLists = postLists;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.custom_list_layout,null);

        TextView titleText = (TextView) convertView.findViewById(R.id.titleTextViewId);
        TextView descText = (TextView) convertView.findViewById(R.id.descriptionTextViewId);
        ImageView postImage = (ImageView) convertView.findViewById(R.id.postImageViewId);

        titleText.setText(postLists.get(position).getPostTitle());
        //Log.e("POST TITLE : ",postLists.get(position).getPostTitle());
        descText.setText(postLists.get(position).getPostDescription());
        Picasso.with(context).load(postLists.get(position).getPostImage()).into(postImage);

        return convertView;
    }

}
