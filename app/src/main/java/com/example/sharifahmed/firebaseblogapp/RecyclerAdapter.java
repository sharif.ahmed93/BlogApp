package com.example.sharifahmed.firebaseblogapp;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 03-Jun-17.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    ArrayList<Post>postLists;
    Context context;

    public RecyclerAdapter(ArrayList<Post> postLists, Context context) {
        this.postLists = postLists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.blog_row_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Post singlePost = postLists.get(position);

        holder.postTitleTV.setText(singlePost.getPostTitle());
        holder.postDescTV.setText(singlePost.getPostDescription());
        Picasso.with(context).load(singlePost.getPostImage()).into(holder.postImageIV);

    }

    @Override
    public int getItemCount() {
        return postLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView postTitleTV;
        TextView postDescTV;
        ImageView postImageIV;

        public ViewHolder(View itemView) {

            super(itemView);

            postTitleTV = (TextView) itemView.findViewById(R.id.postTitleTextViewId);
            postDescTV = (TextView) itemView.findViewById(R.id.postDescriptionTextViewId);
            postImageIV = (ImageView) itemView.findViewById(R.id.postImageViewId);


        }
    }
}
