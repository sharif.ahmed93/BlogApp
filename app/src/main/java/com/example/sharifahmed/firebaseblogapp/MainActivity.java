package com.example.sharifahmed.firebaseblogapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView postListRV;
    ListView postListView;
    ArrayList<Post>postLists;

    RecyclerAdapter recyclerAdapter;
    PostListAdapter postListAdapter;

    private DatabaseReference databaseReferenceUsers;
    private DatabaseReference databaseReferenceBlog;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        databaseReferenceBlog = FirebaseDatabase.getInstance().getReference("blog");

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");
        databaseReferenceUsers.keepSynced(true);

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser() == null){
                    Intent registerIntent = new Intent(MainActivity.this,SignInActivity.class);
                    registerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(registerIntent);
                }
            }
        };

       // postListView = (ListView) findViewById(R.id.postListViewId);

        postListRV = (RecyclerView) findViewById(R.id .postListRecyclerViewId);
        postListRV.setHasFixedSize(true);
        postListRV.setLayoutManager(new LinearLayoutManager(this));

        postLists = new ArrayList<>();


        checkUserExist();

    }

    @Override
    protected void onStart() {

        super.onStart();

        firebaseAuth.addAuthStateListener(authStateListener);
        databaseReferenceBlog.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                postLists.clear();

                for(DataSnapshot singlePostSnapshot : dataSnapshot.getChildren())
                {
                    Post post = singlePostSnapshot.getValue(Post.class);
                    postLists.add(post);
                }

                recyclerAdapter = new RecyclerAdapter(postLists,getApplicationContext());
                postListRV.setAdapter(recyclerAdapter);

               // postListAdapter = new PostListAdapter(MainActivity.this,postLists);
              //  postListView.setAdapter(postListAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    //******************For Menu******************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.actionAddId) {

            Intent intent = new Intent(MainActivity.this,PostActivity.class);
            startActivity(intent);

        }else if (item.getItemId() == R.id.actionSettingsId) {

        }else if (item.getItemId() == R.id.actionLogOutId) {

            logOut();

        }

        return super.onOptionsItemSelected(item);
    }

    private void logOut() {

        firebaseAuth.signOut();

    }
//******************End Menu Work******************************

    private void checkUserExist() {


        if(firebaseAuth.getCurrentUser() != null){
            databaseReferenceUsers.addValueEventListener(new ValueEventListener() {
                final String userId = firebaseAuth.getCurrentUser().getUid();
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.hasChild(userId)) {
                        Intent signInIntent = new Intent(MainActivity.this, SetupActivity.class);//Instead of SetupActivity for SignInActivity Vice Versa
                        signInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(signInIntent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        else{
            Intent signInIntent = new Intent(MainActivity.this, SignInActivity.class);//Instead of SetupActivity for SignInActivity Vice Versa
            signInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(signInIntent);
        }


    }



}
