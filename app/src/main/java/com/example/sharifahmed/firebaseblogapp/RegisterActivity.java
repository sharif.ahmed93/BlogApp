package com.example.sharifahmed.firebaseblogapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class RegisterActivity extends AppCompatActivity {

    private EditText nameET;
    private EditText emailET;
    private EditText passwordET;
    private Spinner userTypeSP;
    private Button signUpBTN;
    private ImageButton photoIB;

    private static final int GALLARY_REQUEST = 2;

    Uri imageUri = null;

    ProgressDialog progressDialog;

    DatabaseReference registerDatabase;
    StorageReference registerStorage;
    FirebaseAuth registerAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nameET = (EditText) findViewById(R.id.nameEditTextId);
        emailET = (EditText) findViewById(R.id.emailEditTextId);
        passwordET = (EditText) findViewById(R.id.passwordEditTextId);
        userTypeSP = (Spinner) findViewById(R.id.userTypeSpinnerId);
        signUpBTN = (Button) findViewById(R.id.signUpButtonId);
        photoIB = (ImageButton) findViewById(R.id.photoImageButtonId);

        progressDialog = new ProgressDialog(this);

        registerDatabase = FirebaseDatabase.getInstance().getReference("users");
        registerStorage = FirebaseStorage.getInstance().getReference("user_images");
        registerAuth = FirebaseAuth.getInstance();

        signUpBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userSignUp();
            }
        });

        photoIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gallaryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                gallaryIntent.setType("image/*");
                startActivityForResult(gallaryIntent,GALLARY_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLARY_REQUEST && resultCode == RESULT_OK){
            imageUri = data.getData();
            photoIB.setImageURI(imageUri);
        }
    }

    private void userSignUp() {

        final String name = nameET.getText().toString().trim();
        final String email = emailET.getText().toString().trim();
        final String password = passwordET.getText().toString().trim();
        final String userType = userTypeSP.getSelectedItem().toString();

        if(!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && imageUri!=null && !userType.equals("Select User Type")){

            progressDialog.setMessage("Signing up......");
            progressDialog.show();

            registerAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if(task.isSuccessful()){

                        StorageReference filePathUser = registerStorage.child(imageUri.getLastPathSegment());

                        filePathUser.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                //String userId =  registerDatabase.push().getKey();
                                String userId = registerAuth.getCurrentUser().getUid();

                                User user = new User(userId,name,email,password,downloadUrl.toString(),userType);

                                registerDatabase.child(userId).setValue(user);

                                progressDialog.dismiss();

                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });

                    }
                }
            });

        }else if (password.length() < 6){
            Toast.makeText(RegisterActivity.this, "Password should be at least 6 charecters", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
        else{
            Toast.makeText(RegisterActivity.this, "Fields are not be empty", Toast.LENGTH_SHORT).show();
        }


    }
}
